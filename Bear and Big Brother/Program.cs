﻿using System;
 
namespace BearandBigBrother
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] t = Console.ReadLine().Split();
            int a = int.Parse(t[0]);
            int b = int.Parse(t[1]);
            int yearCount=0;
            while (true)
            {
                if (a>b)
                    break;
                yearCount++;
                a*=3;
                b*=2;
            }
            System.Console.WriteLine(yearCount);
        }
    }
}