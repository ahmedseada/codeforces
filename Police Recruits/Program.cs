﻿using System;

namespace Police_Recruits
{
    class Program
    {
        static void Main(string[] args)
        {
            int noevents = int.Parse(Console.ReadLine());
            string[] events = Console.ReadLine().Split(' ');
            int crimes = 0, hired = 0;
            for (int i = 0; i < noevents; i++)
            {
                if (num(events[i]) < 0 && hired <= 0)
                    crimes++;
                else if (num(events[i]) < 0 && hired > 0)
                    hired--;
                else if (num(events[i]) > 0)
                    hired+=num(events[i]);
            }
            System.Console.WriteLine(crimes);
        }
        static int num(string number) => int.Parse(number);
    }
}
