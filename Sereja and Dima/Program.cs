﻿using System;
namespace SerejaandDima
{
    class Program
    {
        static void Main(string[] args)
        {
            int ncards = int.Parse(Console.ReadLine());
            string[] cards = Console.ReadLine().Split(' ');
            int serja = 0, dima = 0, rule = 0, lastindex = cards.Length - 1;
            for (int i = 0; i < ncards; i++)
            {
                if (i == lastindex && rule == 0)
                {
                    serja += num(cards[i]);
                    i=ncards-1;
                }
                else if (i == lastindex && rule == 1)
                {
                    dima += num(cards[i]);
                    i=ncards-1;
                }
                else
                {
 
                    if (rule == 0)
                    {
                        if (num(cards[i]) >= num(cards[lastindex]))
                        {
                            serja += num(cards[i]);
                        }
                        else if (num(cards[i]) < num(cards[lastindex]))
                        {
                            serja += num(cards[lastindex]);
                            lastindex--;
                            i = i - 1;
                        }
                        rule = 1;
                    }
                    else
                    {
                        if (num(cards[i]) >= num(cards[lastindex]))
                        {
                            dima += num(cards[i]);
                        }
                        else if (num(cards[i]) < num(cards[lastindex]))
                        {
                            dima += num(cards[lastindex]);
                            lastindex--;
                            i = i - 1;
                        }
                        rule = 0;
                    }
 
                }
            }
            System.Console.WriteLine(serja + " " + dima);
        }
        public static int num(string num) => int.Parse(num);
    }
}