﻿using System;
 
namespace gravityflip
{
    class Program
    {
        static void Main(string[] args)
        {
            int nolines = int.Parse(Console.ReadLine());
            string[] magnets = new string[nolines];
            int group = 1;
            for (int i = 0; i < nolines; i++)
            {
                magnets[i] = Console.ReadLine();
            }
            for (int i = 1; i < nolines; i++)
            {
                if (magnets[i-1][1] == magnets[i][0]) 
                    group++;
                else
                    continue;
            }
            System.Console.WriteLine(group);
 
 
        }
 
    }
}