﻿using System;
 
namespace ateam
{
    class Program
    {
        static void Main(string[] args)
        {
            int noproblems = int.Parse(Console.ReadLine());
            int counter=0;
            for (int i = 0; i < noproblems; i++)
            {
                var line = Console.ReadLine().Split(' ');
                int agree =0;
                for (int k = 0; k < line.Length; k++)
                {
                    if(int.Parse(line[k])==1)
                            agree++;
                }
                if(agree>1) 
                    counter++;
            } 
            System.Console.WriteLine(counter);
        }
      
    }
}