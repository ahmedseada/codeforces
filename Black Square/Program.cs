﻿using System;

namespace Black_Square
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] calories = Console.ReadLine().Split(' ');
            string strips = Console.ReadLine();
            int wastedCalories = 0;
            for (int i = 0; i < strips.Length; i++)
            {
                if (strips[i] == '1')
                    wastedCalories += int.Parse(calories[0]);
                else if (strips[i] == '2')
                    wastedCalories += int.Parse(calories[1]);
                else if (strips[i] == '3')
                    wastedCalories += int.Parse(calories[2]);
                else if (strips[i] == '4')
                    wastedCalories += int.Parse(calories[3]);
            }
            System.Console.WriteLine(wastedCalories);
        }
    }
}
