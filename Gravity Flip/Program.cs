﻿using System;
 
namespace gravityflip
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            var columns = Console.ReadLine().Split(' ');
            var narray = new int[n];
            for (int i = 0; i < n; i++)
            {
                narray[i]=int.Parse(columns[i]);
            }
            Array.Sort(narray);
            for (int i = 0; i < n; i++)
            {
                System.Console.Write(narray[i]+" ");
            }
        }
     
    }
}