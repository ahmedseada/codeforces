﻿using System;
 
namespace gravityflip
{
    class Program
    {
        static void Main(string[] args)
        {
            var username= Console.ReadLine();
            var distinct="";
            for (int i = 0; i < username.Length; i++)
            {
                if (!distinct.Contains(username[i]+""))
                    distinct+=username[i];
            }
            System.Console.WriteLine(distinct.Length % 2 == 0 ? "CHAT WITH HER!" : "IGNORE HIM!");
        }
     
    }
}