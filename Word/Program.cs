﻿using System;
 
namespace gravityflip
{
    class Program
    {
        static void Main(string[] args)
        {
            var output = Console.ReadLine();
            var word =output.ToCharArray();
            int sm = 0, up = 0;
            for (int i = 0; i < word.Length; i++)
            {
                if (char.IsUpper(word[i]))
                    up++;
                else
                    sm++;
            }
            if (sm > up || sm == up)
                System.Console.WriteLine(output.ToLower());
            else
                System.Console.WriteLine(output.ToUpper());
        }
 
    }
}