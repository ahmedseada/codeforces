﻿using System;
 
namespace StonesontheTable
{
    class Program
    {
        static void Main(string[] args)
        {
            int nstones = int.Parse(Console.ReadLine());
            string stones = Console.ReadLine();
            string str = "";
            for (int i = 0; i < nstones; i++)
            {
                for (int k = i + 1; k < nstones; k++)
                {
                    if (stones[i] == stones[k])
                    {
                        str += stones[k];
                        if (k == nstones - 1)
                        {
                            i = k;
                        }
                    }
                    else
                    {
                        i = k - 1;
                        break;
                    }
                }
            }
            System.Console.WriteLine(str.Length);
        }
    }
}
